let score = 0
let itemsOwned = {}
let perSecondIncrease = 0;


let availableItems = {
  Santa:{
    cost: 1000000,
    benefit:10000,
    name: 'Santa'
  },
  MissileSilo:{
    cost: 150000,
    benefit:4096,
    name: 'Missile Silo'
  },
  Chickens:{
    cost: 75000,
    benefit:2048,
    name: 'Chickens'
  },
  Guns:{
    cost: 25000,
    benefit:1024,
    name: 'Guns'
  },
  Medicine:{
    cost: 12000,
    benefit:256,
    name: 'Medicine'
  },
  WaterPump:{
    cost: 1100,
    benefit:16,
    name: 'Water Pump'
  },
  CanOfBeans:{
    cost: 100,
    benefit:4,
    name:'Can'
  },
  Cursor:{
    cost:15,
    benefit:1,
    name:'Cursor'
  }
}




function handleClick() {
  score++
  updateScore()
}

function updateScore() {
  $('.Score').contents().first().replaceWith(`Days Survived: ${Math.round(score)}`)
  $('.PerSecond').contents().first().replaceWith(`DAYS/SEC: ${perSecondIncrease}`)
  $('.GameTitle').contents().first().replaceWith(`${Math.floor(score)} days survived`)
}

function showAvailableItemsInStore() {
  $('.Store').empty()
  for (let key in availableItems) {
    if (Math.round(availableItems[key].cost)<score) {
      $('.Store').prepend($(`<div onclick='buyItem("${key}")' class='Title StoreItem'><h3 >${availableItems[key].name}: <p style="color:green; display:inline-block" > Cost ${Math.round(availableItems[key].cost)}</p></h3></div>`));
    }else{
      $('.Store').prepend($(`<div onclick='buyItem("${key}")' class='Title StoreItem'><h3 >${availableItems[key].name}: <p style="color:red; display:inline-block" > Cost ${Math.round(availableItems[key].cost)}</p></h3></div>`));
    }
  }
}

function showItemsOwned() {
  $('.Inventory').empty()
  for (let key in itemsOwned) {
    $('.Inventory').prepend($(`<div class='Title InventoryItem'><h3>${key}: ${itemsOwned[key]}</h3></div>`));

    for (var i = 0; i < itemsOwned[key]; i++) {
      console.log(key)
      if (key === "Cursor") {
        $('.Inventory').prepend($(`<img class='animated rotateIn' src="./assets/cursor.png" />`));
      }
      else if (key === 'Can') {
        console.log('hello')
        $('.Inventory').prepend($(`<img class='animated wobble' src="./assets/canofbeans.png" />`));
      }
      else if (key === "Water Pump") {
        $('.Inventory').prepend($(`<img class='animated rubberBand' src="./assets/waterpump.png" />`));
      }
      else if (key === "Medicine") {
        $('.Inventory').prepend($(`<img class='animated rubberBand' src="./assets/medicine.png" />`));
      }
      else if (key === "Guns") {
        $('.Inventory').prepend($(`<img class='animated rubberBand' src="./assets/guns.png" />`));
      }
      else if (key === "Chickens") {
        $('.Inventory').prepend($(`<img class='animated rubberBand' src="./assets/chickens.png" />`));
      }
      else if (key === "Missile Silo") {
        $('.Inventory').prepend($(`<img class='animated rubberBand' src="./assets/missile.png" />`));
      }
      else if (key === "Santa") {
        $('.Inventory').prepend($(`<img class='animated rubberBand' src="./assets/santa.png" />`));
      }
  }
}
}

/**
Adds a bought item to the inventory
**/
function addItem(item) {
  if (typeof(itemsOwned[item]) === 'undefined') {
    itemsOwned[item] = 1
  } else {
    itemsOwned[item]++
  }
  showItemsOwned();
  showAvailableItemsInStore();
}

function buyItem(itemID){
  let itemPrice = availableItems[itemID].cost;
  if (score >= itemPrice) {
    let clickBenefitPerSecond = availableItems[itemID].benefit;
    //adjust score for purchase
    score-=itemPrice;
    //increase rate of score
    perSecondIncrease+=clickBenefitPerSecond;
    //get a factor of the current price
    let costIncrease = itemPrice*.07;
    //increase the price
    availableItems[itemID].cost +=costIncrease;
    //add item to Inventory
    addItem(availableItems[itemID].name)
  }
}

function updateScoreByFactor() {
  if (perSecondIncrease > 0) {
    score += perSecondIncrease;
    updateScore()
  }
}



setInterval(updateScoreByFactor, 1000)
setInterval(showAvailableItemsInStore, 1000)
